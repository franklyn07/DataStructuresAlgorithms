import random 
import pydot
import os
import functools
import Image

#This class will hold the node structure and methods attributed to the node.
class Node:
    
	#state index
    index = -1
    
    #transitions
    a =-1
    b =-1

    #accept state
    acceptState = False

    #start state
    startState = False

    #visited node
    visited = False

    #constructor used when generalising and randomising
    def __init__(self, indexAssigned):
        self.index = indexAssigned
        self.acceptState = self.isAccept()

    #deciding randomly whether state will be accept or not
    def isAccept(self):
    	randomVal = random.random()
    	if randomVal >= 0.5:
    		return True
    	else:
    		return False

#This will be the main method for execution
def main():
	numberOfStates = random.randrange(8,16,1)

	#contains a list of nodes
	listOfNodes = []

	#contains a dictionary of transitions between nodes
	dictOfTransitions = {}

	#create list of nodes and store them in a list
	for index in xrange(0,numberOfStates):
		createdNode = createNode(numberOfStates,index)
		
		#inserting node in list
		listOfNodes.append(createdNode)

		#inserting transitions pertaining to new node in dictionary
		dictOfTransitions[createdNode.index] = (createdNode.a,createdNode.b)

	#setting start state and returning it for graph
	root = setStartState(numberOfStates,listOfNodes)

	#generating graph using list of nodes
	generateGraph(listOfNodes, "./Artifacts/GraphUnvisitedStates.png")

	#getting maximum depth of tree
	maximumDepth = getMaxDepthBFS(listOfNodes, dictOfTransitions, root)
	print "Maximum Depth of Unoptimised Tree: " , maximumDepth

	#removing unvisited states and setting the visited ones back to false	
	cleanUp(listOfNodes)

	#new dictionary with unvisited states removed
	dictOfTransitions = updateDictionary(listOfNodes)

	#generating graph using dictionary of transitions
	generateGraph(listOfNodes, "./Artifacts/GraphCleanedOfUnvisitedStates.png")

	#will store index assigned to start state after minimisation
	global newStartState
	#producing list of nodes of minimised DFA using hopcroft algorithm
	minimisedDFA = produceMinimisedDFA(minimiseDFA(dictOfTransitions, listOfNodes), dictOfTransitions,listOfNodes, root)
	
	#producing dictionary of minimised DFA
	dictOfTransitionsMinimised = updateDictionary(minimisedDFA)

	#generating graph for minimised DFA
	generateGraph(minimisedDFA, "./Artifacts/MinimisedDFA.png")

	#getting maximum depth of minimised tree
	maximumDepthMinDFA = getMaxDepthBFS(minimisedDFA, dictOfTransitionsMinimised, newStartState)
	print "Maximum Depth of Minimised Tree: " , maximumDepthMinDFA

	#updating indexes and transitions after minimising
	listOfNodes,dictOfTransitionsMinimised = updateIndex(minimisedDFA)

	#producing graph with updated indices
	generateGraph(minimisedDFA, "./Artifacts/UpdatedIndicesAfterMin.png")

	#finding number of SCC in minimised graph
	TarjanSCC(minimisedDFA,dictOfTransitionsMinimised)

#dfa minimisation using efficient hopcroft
def minimiseDFA(dictOfTransitions,listNodes):
    listOfPartitions = [set() for _ in range(2)]
    seperateFinalFromNonFinal(listNodes,listOfPartitions)

    workingSets = [set(listOfPartitions[1])]

    dictionaryItems = dictOfTransitions.items()

    while len(workingSets)!=0:
        setA = workingSets.pop()
        for indexOfTransition in range(2):
            setX = set()
            for currentItem in dictionaryItems:
                if (currentItem[1])[indexOfTransition] in setA:
                    setX.add(currentItem[0])
            for currentSet in reversed(listOfPartitions):
                setIntersection = currentSet & setX
                setDifference = currentSet-setX
                if len(setIntersection) != 0 and len(setDifference) != 0:
                    listOfPartitions.remove(currentSet)
                    listOfPartitions.append(setDifference)
                    listOfPartitions.append(setIntersection)
                    if checkWait(currentSet, workingSets) == True:
                        workingSets.remove(currentSet)
                        workingSets.append (setDifference)
                        workingSets.append(setIntersection)
                    else:
                        if len(setIntersection) <= len(setDifference):
                            workingSets.append(setIntersection)
                        else:
                            workingSets.append(setDifference)

    return listOfPartitions

#check whether a set is in waiting list through set equality
#this could have been avoided if we used a set of sets rather than a list of sets
#however due to time constraints it was not updated
def checkWait(currentSet,myList):
	for currentItem in myList:
		if currentItem <= currentSet and currentSet <= currentItem:
			return True
	return False



#sort states as final and non final
def seperateFinalFromNonFinal(listOfNodes, listOfPartitions):
	for node in listOfNodes:
		if node.acceptState == True:
			listOfPartitions[1].add(node.index)
		else:
			listOfPartitions[0].add(node.index)


#produce new dfa (aka list and dictionary) from minimised list (which is a list of paritions/sets)
def produceMinimisedDFA(minimisedList,dictOfTransitions,listOfNodes,startState):
    newListOfNodes = []

    global newStartState

    for index in range(len(minimisedList)):
        if len(minimisedList[index])>0:
            createdNode = Node(index)
            #pop any item from set
            nodeIndex = minimisedList[index].pop()
            #putting it back in for search and integrity purposes
            minimisedList[index].add(nodeIndex)
            createdNode.acceptState = isAccept(nodeIndex,listOfNodes) 
            tupleOfTrans = dictOfTransitions.get(nodeIndex)
            createdNode.a = returnIndexNewDFA(tupleOfTrans[0],minimisedList)
            createdNode.b = returnIndexNewDFA(tupleOfTrans[1],minimisedList)
            #check if start state
            if startState == nodeIndex:
                newStartState = index
                createdNode.startState = True
            else:
                #if not exhaust current set of states and check if it is one of them
                #and after you're finished put them back in
                listOfStates = [nodeIndex]
                while len(minimisedList[index])!=0:
                    currentIndex = minimisedList[index].pop()
                    listOfStates.append(currentIndex)
                    if currentIndex == startState:
                        newStartState = currentIndex
                        createdNode.startState = True
                        break
                #putting back items in set
                for currentSet in reversed(listOfStates):
                    minimisedList[index].add(listOfStates.pop())
            newListOfNodes.append(createdNode)

    return newListOfNodes

#returns whether a node is an accept or non accept state
def isAccept(index,listOfNodes):
	for node in listOfNodes:
		if node.index == index:
			return node.acceptState

#returns at what new index in the minimised DFA the transition leads to
def returnIndexNewDFA(transitionState, minimisedList):
	for i in range(len(minimisedList)):
		if transitionState in minimisedList[i]:
			return i
	return -1

#updates dransitionsictionary of transitions
def updateDictionary(listOfNodes):
	newDictOfTrans = {}

	#generating nodes
	for node in listOfNodes:
		newDictOfTrans[node.index] = (node.a,node.b)

	return newDictOfTrans

#updates list of nodes
def updateIndex(listOfNodes):
	newListOfNodes = []
	indexMap = {}

	#iterating through old list of nodes to create map from old index to new index
	#also create new nodes excluding the transitions 
	for i in range (len(listOfNodes)):
		oldNode = listOfNodes[i];
		#create new node with new index, however same accept and start state of old
		newListOfNodes.append(Node(i))
		newListOfNodes[i].acceptState = oldNode.acceptState
		newListOfNodes[i].startState = oldNode.startState
		#create link between old index and new index
		indexMap[oldNode.index] = i

	#update old transitions to new equivalent indexes in newListOfNodes
	for i in range(len(listOfNodes)):
		oldNode = listOfNodes[i]
		#get old transition a and search for its equivalent index
		newA = indexMap.get(oldNode.a,-1)
		#if error print out else assing it
		if (newA == -1):
			print "Error in converting indexes"
		else:
			newListOfNodes[i].a = newA

		#get old transition b and search for its equivalent index
		newB = indexMap.get(oldNode.b,-1)
		#if error print out else assing it
		if (newB == -1):
			print "Error in converting indexes"
		else:
			newListOfNodes[i].b = newB

	#after updating indexes, update dict of transitions and return new list of nodes as well
	#as updated dictionary

	return newListOfNodes, updateDictionary(newListOfNodes)


#This will get a random node from the indexes available
def getNode(numberOfStates):
	return random.randrange(0,numberOfStates,1)

#creates a node with the appropriate transitons -
#whether it will be a start state or not
def createNode(numberOfStates,currentIndex):
	createdNode = Node(currentIndex)
	createdNode.a = getNode(numberOfStates)
	createdNode.b = getNode(numberOfStates)
	return createdNode

#Finds node which will be set as start state, in a random  manner 
#and sets it accordingly
def setStartState(numberOfStates,listOfNodes):
	start = getNode(numberOfStates)
	for node in listOfNodes:
		if node.index == start:
			node.startState = True
	return start

#method used to produce a visual artifact of the random graph for testing purposes
def generateGraph(nodeList, graphName):
	#creating my graph object
	myGraph = pydot.Dot(graph_type = "digraph")

	currentIndex = 0

	#generating nodes
	for node in nodeList:
		#adding node to myGraph
		#if start state and is also accept state I want it blue
		if node.startState == True and node.acceptState == True:
			currentNode = pydot.Node(node.index,style = "filled", fillcolor ="blue")
		#if just accept state i want it green
		elif node.acceptState == True:
			currentNode = pydot.Node(node.index,style = "filled", fillcolor ="green")
		#if just start state i want it red
		elif node.startState == True:
			currentNode = pydot.Node(node.index,style = "filled", fillcolor ="red")
		#else i want it white
		else:
			currentNode = pydot.Node(node.index,style = "filled", fillcolor ="white")

		myGraph.add_node(currentNode)

		#adding edges to myGraph
		currentEdge = pydot.Edge(currentNode,node.a)
		myGraph.add_edge(currentEdge)
		currentEdge = pydot.Edge(currentNode,node.b)
		myGraph.add_edge(currentEdge)	

	#generating png
	myGraph.write_png(graphName)

#returns the maximumm depth of the graph using a bfs
def getMaxDepthBFS(listOfNodes, dictOfTransitions,startNode):
	queue = []
	currentMax = 0
	queue.append(startNode)
	searchAndChange(listOfNodes,startNode)
	#takes copy of list
	queueCpy = list(queue)

	#if only one node no need to iterate, just return 0
	if len(listOfNodes) <= 1:
		return currentMax

	while(len(queue)!=0):
		#compare the current queue with the previous checkpoint
		#if nothing is in common it means a new depth is reached
		#thus take a new checkpoint and increase depth
		if(compareLists(queue,queueCpy) == True):
			queueCpy = list(queue)
			currentMax+=1
		currentNode = queue.pop(0)
		tupleOfTransitions = dictOfTransitions.get(currentNode,-1)
		i = 0
		while (i < 2):
			if tupleOfTransitions == -1:
				print "Error in Finding Max Depth"
				print currentNode
				print dictOfTransitions
			else:
				if searchAndChange(listOfNodes,tupleOfTransitions[i])==True:
					queue.append(tupleOfTransitions[i])
			i+=1

	return currentMax

#searches for a node in the list and changes their value to visited		
def searchAndChange(myList,index):
	for node in myList:
		if(node.index == index and node.visited == False):
			node.visited = True
			return True
	return False

#compares two lists and returns true if they have nothing in common
def compareLists(list1,list2):
	if(list(set(list1).intersection(list2)) == []):
		return True
	else:
		return False

#delete unreachable and clear those reachable and visited
def cleanUp(listNodes):
	for node in reversed(listNodes):
		if node.visited == False:
			listNodes.remove(node)
		else: 
			node.visited = False

#Implementation of Tarjan's algorithm in linear time
def TarjanSCC(listOfNodes,dictOfTransitions):
	#holds length of graph in terms of nodes
	n = len(listOfNodes)
	
	#holds the id we will assign to the nodes (for SCC)
	#will determine whether a node is unvisited or not
	TarjanSCC.nodeVal = 0

	#will hold the number of scc encountered
	TarjanSCC.sccCount = 0

	TarjanSCC.minimum = 999999
	TarjanSCC.maximum = 0
	TarjanSCC.counter = 0

	#list of the ids assigned to each node
	ids = []

	#initialise all with -1 aka unvisited
	for i in range(n):
		ids.append(-1)

	#list of the lowest value of each node
	#this will determine the scc since nodes with same lowvalue are
	#in same scc
	lowValue = []

	#initialise all with a 0
	for i in range(n):
		lowValue.append(0)

	#will hold whether a node is on stack
	onStack = []

	#initialise all the nodes to be off the stack
	for i in range(n):
		onStack.append(False); 

	#using a list as a stack to hold history of our current nodes whilst trying to find scc
	stack = []

	#will return a list of all the low values found in scc
	def getLowValues():
		for i in range(n):
			if ids[i] == -1:
				dfs(i)

	#will perform a dfs on current id to find scc 
	def dfs(current):

		#updating data of lists with current node
		stack.append(current)
		onStack[current] = True
		ids[current] = lowValue[current] = TarjanSCC.nodeVal
		TarjanSCC.nodeVal +=1

		#visit all the neighbours of the current node
		tupleOfNeighbours = dictOfTransitions.get(current,-1)
		#if dictionary didn't find current id an error occurred
		if tupleOfNeighbours == -1:
			print "Error"
			return
		else:
			#go through both neighbours
			for i in range(2):
				#if current neighbour is unvisited perform dfs on it
				if(ids[tupleOfNeighbours[i]])==-1:
					dfs(tupleOfNeighbours[i])
				#STAR*::on rollback check an id is already on stack, if it is
				#there is a cycle thus an scc - set lowValue of current  and neighbour
				#to the lowest value between them
				if(onStack[tupleOfNeighbours[i]]):
					lowValue[current] = min(lowValue[current],lowValue[tupleOfNeighbours[i]])


			#check if the current is in an scc
			if ids[current] == lowValue[current]:
				TarjanSCC.sccCount+=1
				#always empty stack of nodes in scc
				TarjanSCC.counter = 0
				while True:
					TarjanSCC.counter += 1
					idNode = stack.pop()
					print idNode, current
					onStack[idNode] = False
					#sets all the nodes in scc to the same low value as parent in scc
					#note parent always contains lowest value of all the nodes in scc at first due
					#to STAR*
					lowValue[idNode] = TarjanSCC.nodeVal
					if idNode == current:
						print TarjanSCC.counter
						#check if count(counts number of nodes in that scc) is minimum or maximum
						if TarjanSCC.counter > TarjanSCC.maximum:
							TarjanSCC.maximum = TarjanSCC.counter
						if TarjanSCC.counter < TarjanSCC.minimum:
							TarjanSCC.minimum = TarjanSCC.counter
						break


	#call get low values func
	getLowValues()

	print lowValue

	#print number of strongly connected graphs
	print "Number of Strongly Connected Graphs = ", TarjanSCC.sccCount
	
	#if equal to zero it means there is one scc made up of all nodes
	if (TarjanSCC.sccCount == 1):
		TarjanSCC.minimum = TarjanSCC.maximum = len(listOfNodes)

	#printing maximum size and minimum size of scc
	print "Smallest Strongly Connected Graph Consists of ",TarjanSCC.minimum, " nodes!"
	print "Largest Strongly Connected Graph Consists of ",TarjanSCC.maximum, " nodes!"


main()

#inefficient dfa minimisation
# #minimises DFA using Hopcroft
# def minimiseDFA2(dictOfTransitions, listNodes):
	#this list will contain the sets of states seperated - we start with 2, one for final and one for non final
	# listOfSeparationSets = [set() for _ in range(2)]

	# seperateFinalFromNonFinal(listNodes,listOfSeparationSets)

	# currentCopyOfListOfSeperation = list(listOfSeparationSets)
	# while True:
	# 	localListOfSets = []
	# 	for currentSet in currentCopyOfListOfSeperation:
	# 		if len(currentSet) > 1:
	# 			localListOfSets += compareTransitions(currentSet,currentCopyOfListOfSeperation,dictOfTransitions)
	# 		else:
	# 			localListOfSets.append(currentSet)
	# 	if localListOfSets != currentCopyOfListOfSeperation:
	# 		currentCopyOfListOfSeperation = list(localListOfSets)
	# 	else:
	# 		return localListOfSets

#return list of sets after comparing transitions
# def compareTransitions(mySet,listOfSeparationSets,dictOfTransitions):
# 	ListOfSets = []
# 	localSet = set()

# 	#get next value from set
# 	firstValFromSet = next(iter(mySet))
# 	localSet.add(firstValFromSet)
# 	tupleOfTransitions = dictOfTransitions.get(firstValFromSet,-1)
# 	state1 = tupleOfTransitions[0]
# 	state2 = tupleOfTransitions[1]

# 	#compare each value in the current set with first value
# 	#if the transitions lead to states in the same set, add the value to the local working set
# 	for currentValFromSet in mySet:
# 		flag = 0 
# 		currentTupleOfTransitions = dictOfTransitions.get(currentValFromSet,-1)
# 		currentState1 = currentTupleOfTransitions[0]
# 		currentState2 = currentTupleOfTransitions[1]
# 		for currentSet in listOfSeparationSets:
# 			if (state1 in currentSet and currentState1 in currentSet):
# 				flag +=1
# 			if (state2 in currentSet and currentState2 in currentSet):
# 				flag +=1
# 			if flag == 2:
# 				localSet.add(currentValFromSet)
# 				break
		
# 		#if not both in the same state and no other new partitions, create a new partition with
# 		#the current value			
# 		if flag != 2 and len(ListOfSets) == 0:
# 			newSet = set()
# 			newSet.add(currentValFromSet)
# 			ListOfSets.append(newSet)
# 		#if there are some partitions created check if current value leads to transitions in those
# 		#partitions
# 		elif flag!=2:
# 			for currentSet in ListOfSets:
# 				flag = 0
# 				firstValCurrSet = next(iter(currentSet))
# 				localTupleOfTransitions = dictOfTransitions.get(firstValCurrSet,-1)
# 				localState1 = localTupleOfTransitions[0]
# 				localState2 = localTupleOfTransitions[1]
# 				for currentSet1 in listOfSeparationSets:
# 					if (localState1 in currentSet1 and currentState1 in currentSet1):
# 						flag+=1
# 					if (localState2 in currentSet1 and currentState2 in currentSet1):
# 						flag+=1
# 					if flag ==2:		
# 						currentSet.add(currentValFromSet)
# 						break
			
# 			#if not, create a new partition
# 			if flag!=2:
# 				newSet = set()
# 				newSet.add(currentValFromSet)
# 				ListOfSets.append(newSet)

# 	ListOfSets.append(localSet)	
# 	return ListOfSets