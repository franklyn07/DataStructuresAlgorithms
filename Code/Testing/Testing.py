import random 
import pydot
import os
import functools
import Image

#This class will hold the node structure and methods attributed to the node.
class Node:
    
	#state index
    index = -1
    
    #transitions
    a =-1
    b =-1

    #accept state
    acceptState = False

    #start state
    startState = False

    #visited node
    visited = False

    def __init__(self, indexAssigned, accept, start,a,b):
        self.index = indexAssigned
        self.acceptState = accept
        self.startState = start
        self.a = a
        self.b = b


def main ():
    listOfNodes = []
    dictOfTrans = {}

    listOfNodes.append(Node(1,False,False,1,3))
    listOfNodes.append(Node(2,False,False,1,1))
    listOfNodes.append(Node(3,False,False,2,5))
    listOfNodes.append(Node(5,False,False,8,6))
    listOfNodes.append(Node(6,False,False,8,7))
    listOfNodes.append(Node(7,False,False,10,10))
    listOfNodes.append(Node(8,False,False,10,9))
    listOfNodes.append(Node(9,False,False,5,11))
    listOfNodes.append(Node(10,False,False,9,11))
    listOfNodes.append(Node(11,False,False,12,14))
    listOfNodes.append(Node(12,False,False,13,13))
    listOfNodes.append(Node(13,False,False,11,15))
    listOfNodes.append(Node(14,False,False,13,13))
    listOfNodes.append(Node(15,False,False,14,14))

    generateGraph(listOfNodes, "TestGraph2")
    listOfNodes,dictOfTrans = updateIndex(listOfNodes)

    print dictOfTrans

    generateGraph(listOfNodes, "TestGraph")

    TarjanSCC(listOfNodes,dictOfTrans)


#updates dransitionsictionary of transitions
def updateDictionary(listOfNodes):
    newDictOfTrans = {}

    #generating nodes
    for node in listOfNodes:
        newDictOfTrans[node.index] = (node.a,node.b)

    return newDictOfTrans

#method used to produce a visual artifact of the random graph for testing purposes
def generateGraph(nodeList, graphName):
    #creating my graph object
    myGraph = pydot.Dot(graph_type = "digraph")

    currentIndex = 0

    #generating nodes
    for node in nodeList:
        #adding node to myGraph
        #if start state and is also accept state I want it blue
        if node.startState == True and node.acceptState == True:
            currentNode = pydot.Node(node.index,style = "filled", fillcolor ="blue")
        #if just accept state i want it green
        elif node.acceptState == True:
            currentNode = pydot.Node(node.index,style = "filled", fillcolor ="green")
        #if just start state i want it red
        elif node.startState == True:
            currentNode = pydot.Node(node.index,style = "filled", fillcolor ="red")
        #else i want it white
        else:
            currentNode = pydot.Node(node.index,style = "filled", fillcolor ="white")

        myGraph.add_node(currentNode)

        #adding edges to myGraph
        currentEdge = pydot.Edge(currentNode,node.a)
        myGraph.add_edge(currentEdge)
        currentEdge = pydot.Edge(currentNode,node.b)
        myGraph.add_edge(currentEdge)   

    #generating png
    myGraph.write_png(graphName)

#dfa minimisation using efficient hopcroft
def minimiseDFA(dictOfTransitions,listNodes):
    listOfPartitions = [set() for _ in range(2)]
    seperateFinalFromNonFinal(listNodes,listOfPartitions)

    workingSets = [set(listOfPartitions[1])]

    dictionaryItems = dictOfTransitions.items()

    while len(workingSets)!=0:
        setA = workingSets.pop()
        for indexOfTransition in range(2):
            setX = set()
            for currentItem in dictionaryItems:
                if (currentItem[1])[indexOfTransition] in setA:
                    setX.add(currentItem[0])
            for currentSet in reversed(listOfPartitions):
                setIntersection = currentSet & setX
                setDifference = currentSet-setX
                if len(setIntersection) != 0 and len(setDifference) != 0:
                    listOfPartitions.remove(currentSet)
                    listOfPartitions.append(setDifference)
                    listOfPartitions.append(setIntersection)
                    if checkWait(currentSet, workingSets) == True:
                        workingSets.remove(currentSet)
                        workingSets.append (setDifference)
                        workingSets.append(setIntersection)
                    else:
                        if len(setIntersection) <= len(setDifference):
                            workingSets.append(setIntersection)
                        else:
                            workingSets.append(setDifference)



    return listOfPartitions

#returns the maximumm depth of the graph using a bfs
def getMaxDepthBFS(listOfNodes, dictOfTransitions,startNode):
    queue = []
    currentMax = 0
    queue.append(startNode)
    searchAndChange(listOfNodes,startNode)
    #takes copy of list
    queueCpy = list(queue)

    #if only one node then max depth is 0
    if len(listOfNodes)==1:
        return currentMax

    while(len(queue)!=0):
        #compare the current queue with the previous checkpoint
        #if nothing is in common it means a new depth is reached
        #thus take a new checkpoint and increase depth
        if(compareLists(queue,queueCpy) == True):
            queueCpy = list(queue)
            currentMax+=1
        currentNode = queue.pop(0)
        tupleOfTransitions = dictOfTransitions.get(currentNode,-1)
        i = 0
        while (i < 2):
            if searchAndChange(listOfNodes,tupleOfTransitions[i])==True:
                queue.append(tupleOfTransitions[i])
            i+=1

    return currentMax

#check whether a set is in waiting list
def checkWait(currentSet,myList):
    for currentItem in myList:
        if currentItem <= currentSet and currentSet <= currentItem:
            return True
    return False

#updates dransitionsictionary of transitions
def updateDictionary(listOfNodes):
    newDictOfTrans = {}

    #generating nodes
    for node in listOfNodes:
        newDictOfTrans[node.index] = (node.a,node.b)

    return newDictOfTrans

#searches for a node in the list and changes their value to visited     
def searchAndChange(myList,index):
    print index
    print myList
    for node in myList:
        print "Index ",node.index
        print "Visited ",node.visited
        if(node.index == index and node.visited == False):
            node.visited = True
            return True
    return False

#sort states as final and non final
def seperateFinalFromNonFinal(listOfNodes, listOfPartitions):
    for node in listOfNodes:
        if node.acceptState == True:
            listOfPartitions[1].add(node.index)
        else:
            listOfPartitions[0].add(node.index)

#produce new dfa from minimised list
def produceMinimisedDFA(minimisedList,dictOfTransitions,listOfNodes,startState):
    newListOfNodes = []
    print minimisedList

    global newStartState

    for index in range(len(minimisedList)):
        if len(minimisedList[index])>0:
            createdNode = Node(index,False, False,-1,-1)
            #pop any item from set
            nodeIndex = minimisedList[index].pop()
            #putting it back in for search and integrity purposes
            minimisedList[index].add(nodeIndex)
            createdNode.acceptState = isAccept(nodeIndex,listOfNodes) 
            tupleOfTrans = dictOfTransitions.get(nodeIndex)
            createdNode.a = returnIndexNewDFA(tupleOfTrans[0],minimisedList)
            createdNode.b = returnIndexNewDFA(tupleOfTrans[1],minimisedList)
            #check if start state
            if startState == nodeIndex:
                newStartState = index
                createdNode.startState = True
            else:
                #if not exhaust current set of states and check if it is one of them
                #and after you're finished put them back in
                listOfStates = [nodeIndex]
                while len(minimisedList[index])!=0:
                    currentIndex = minimisedList[index].pop()
                    listOfStates.append(currentIndex)
                    if currentIndex == startState:
                        newStartState = currentIndex
                        createdNode.startState = True
                        break
                #putting back items in set
                for currentSet in reversed(listOfStates):
                    minimisedList[index].add(listOfStates.pop())
            print createdNode.startState
            newListOfNodes.append(createdNode)

    return newListOfNodes

#returns whether a node is an accept or non accept state
def isAccept(index,listOfNodes):
    for node in listOfNodes:
        if node.index == index:
            return node.acceptState

#returns at what new index in the minimised DFA the transition leads to
def returnIndexNewDFA(transitionState, minimisedList):
    for i in range(len(minimisedList)):
        if transitionState in minimisedList[i]:
            return i
    return -1

#compares two lists and returns true if they have nothing in common
def compareLists(list1,list2):
    if(list(set(list1).intersection(list2)) == []):
        return True
    else:
        return False

#updates list of nodes
def updateIndex(listOfNodes):
    newListOfNodes = []
    indexMap = {}

    #iterating through old list of nodes to create map from old index to new index
    #also create new nodes excluding the transitions 
    for i in range (len(listOfNodes)):
        oldNode = listOfNodes[i];
        #create new node with new index, however same accept and start state of old
        newListOfNodes.append(Node(i,False,False,-1,-1))
        #create link between old index and new index
        indexMap[oldNode.index] = i

    #update old transitions to new equivalent indexes in newListOfNodes
    for i in range(len(listOfNodes)):
        oldNode = listOfNodes[i]
        #get old transition a and search for its equivalent index
        newA = indexMap.get(oldNode.a,-1)
        #if error print out else assing it
        if (newA == -1):
            print "Error in converting indexes"
        else:
            newListOfNodes[i].a = newA

        #get old transition b and search for its equivalent index
        newB = indexMap.get(oldNode.b,-1)
        #if error print out else assing it
        if (newB == -1):
            print "Error in converting indexes"
        else:
            newListOfNodes[i].b = newB

    #after updating indexes, update dict of transitions and return new list of nodes as well
    #as updated dictionary

    return newListOfNodes, updateDictionary(newListOfNodes)

#Implementation of Tarjan's algorithm in linear time
def TarjanSCC(listOfNodes,dictOfTransitions):
    #holds length of graph in terms of nodes
    n = len(listOfNodes)
    
    #holds the id we will assign to the nodes (for SCC)
    #will determine whether a node is unvisited or not
    TarjanSCC.nodeVal = 0

    #will hold the number of scc encountered
    TarjanSCC.sccCount = 0

    #list of the ids assigned to each node
    ids = []

    #initialise all with -1 aka unvisited
    for i in range(n):
        ids.append(-1)

    #list of the lowest value of each node
    #this will determine the scc since nodes with same lowvalue are
    #in same scc
    lowValue = []

    #initialise all with a 0
    for i in range(n):
        lowValue.append(0)

    #will hold whether a node is on stack
    onStack = []

    #initialise all the nodes to be off the stack
    for i in range(n):
        onStack.append(False); 

    #using a list as a stack to hold history of our current nodes whilst trying to find scc
    stack = []

    #will return a list of all the low values found in scc
    def getLowValues():
        for i in range(n):
            if ids[i] == -1:
                dfs(i)

    #will perform a dfs on current id to find scc 
    def dfs(current):

        #updating data of lists with current node
        stack.append(current)
        onStack[current] = True
        ids[current] = lowValue[current] = TarjanSCC.nodeVal
        TarjanSCC.nodeVal +=1

        #visit all the neighbours of the current node
        tupleOfNeighbours = dictOfTransitions.get(current,-1)
        #if dictionary didn't find current id an error occurred
        if tupleOfNeighbours == -1:
            print "Error"
            return
        else:
            #go through both neighbours
            for i in range(2):
                #if current neighbour is unvisited perform dfs on it
                if(ids[tupleOfNeighbours[i]])==-1:
                    dfs(tupleOfNeighbours[i])
                #STAR*::on rollback check an id is already on stack, if it is
                #there is a cycle thus an scc - set lowValue of current  and neighbour
                #to the lowest value between them
                if(onStack[tupleOfNeighbours[i]]):
                    lowValue[current] = min(lowValue[current],lowValue[tupleOfNeighbours[i]])


            #check if the current is in an scc
            if ids[current] == lowValue[current]:
                TarjanSCC.sccCount+=1
                #always empty stack of nodes in scc
                while not stack:
                    idNode = stack.pop()
                    onStack[idNode] = False
                    #sets all the nodes in scc to the same low value as parent in scc
                    #note parent always contains lowest value of all the nodes in scc at first due
                    #to STAR*
                    lowValue[idNode] = nodeVal[current]
                    if idNode == current:
                        break

    #call get low values func
    getLowValues()

    #print number of strongly connected graphs
    print "Number of Strongly Connected Graphs = ", TarjanSCC.sccCount,"\n"

    minimum = 9999999999
    maximum = -1

    #finding maximum size of scc and minimum size of scc
    for i in range(n):
        count = lowValue.count(i)
        if(count > maximum and count != 0):
            maximum = count
        if(count < minimum and count != 0):
            minimum = count

    #fixing a bag where sccCount is right however lowValues has a single
    #trailing value which messes up the statistics      
    if (TarjanSCC.sccCount == 1):
        minimum = maximum = minimum + maximum

    #printing maximum size and minimum size of scc
    print "Smallest Strongly Connected Graph Consists of ",minimum, " nodes!"
    print "Largest Strongly Connected Graph Consists of ",maximum, " nodes!"

    print lowValue

main()